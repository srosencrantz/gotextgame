package main

import (
	"fmt"
	"log"
	"strings"
)

func main() {

eventloop:
	for {
		room(&state, &data)
		switch state.CurrentRoom {
		case "fail":
			fmt.Println(data.Fail)
			break eventloop
		case "success":
			fmt.Println(data.Success)
			break eventloop
		}
	}

}

// Functions for making each room happen
//----------------------------------------------------------------------

func getChoice() (choice string) {
	var err error
	_, err = fmt.Scanln(&choice)
	if err != nil {
		log.Fatal(err)
	}
	return strings.TrimSpace(strings.ToUpper(choice))
}

func room(state *StateData, data *GameData) {
	room := data.RoomMap[state.CurrentRoom]
	fmt.Println("")
	fmt.Println(room.Description)
	if room.Action != nil {
		room.Action(state)
	}
	fmt.Println(data.Prompt)
	for label, choice := range room.ChoiceMap {
		fmt.Printf("    %s: %s\n", label, choice.ChoiceDesc)
	}
	var input string
	for {
		input = getChoice()
		if _, ok := room.ChoiceMap[input]; ok {
			break
		}
		fmt.Println(data.BadOptionPrompt)
	}
	theChoice := room.ChoiceMap[input]
	if theChoice.ResultDesc != "" {
		fmt.Println(theChoice.ResultDesc)
	}
	if theChoice.ResultAction != nil {
		theChoice.ResultAction(state)
	}
	state.CurrentRoom = theChoice.ResultRoom
}

// Some Structs to describe aspects of the game
//-----------------------------------------------------------------------

// Hero describes the properties of our hero, in this case just hitpoints
type Hero struct {
	HP int
}

// Choice describes a choice that can be made in a room, a resulting comment and room
type Choice struct {
	ChoiceDesc   string
	ResultDesc   string
	ResultAction Action
	ResultRoom   string
}

// Action is a function that acts on the current game state
type Action func(*StateData)

// RoomData describes a room and has a map of choices that can be made in the room
type RoomData struct {
	Description string
	Action      Action
	ChoiceMap   map[string]*Choice
}

// GameData describes all the rooms in the game
type GameData struct {
	Success         string
	Fail            string
	Prompt          string
	BadOptionPrompt string
	RoomMap         map[string]*RoomData
}

// StateData describes the current state of the game
type StateData struct {
	Hero        Hero
	CurrentRoom string
}

// The data that describes the game
//------------------------------------------------------------------------

var state = StateData{
	Hero:        Hero{HP: 10},
	CurrentRoom: "airlock"}

var data = GameData{
	Success:         "Having survived the rogue AI and escaped the alien incursion. You relax aboard the accelerating shuttle craft with the Princess.",
	Fail:            "You've died... sorry",
	Prompt:          "Do you want to: ",
	BadOptionPrompt: "Please choose one of the options!",
	RoomMap: map[string]*RoomData{
		"airlock": &RoomData{
			Description: "You are floating outside an airlock in space. There is a green button on the airlock.",
			Action: func(sd *StateData) {
				fmt.Printf("HP: %d\n", sd.Hero.HP)
			},
			ChoiceMap: map[string]*Choice{
				"A": &Choice{
					ChoiceDesc: "Push the button",
					ResultDesc: "",
					ResultAction: func(sd *StateData) {
						sd.Hero.HP++
						fmt.Printf("You're feeling much better, HP: %d\n", sd.Hero.HP)
					},
					ResultRoom: "hall"},
				"B": &Choice{
					ChoiceDesc: "Float away from the airlock",
					ResultDesc: "You slowly float away into the infinite vacuum of space, and have plenty of time to think about your life choices.",
					ResultRoom: "fail"}}},
		"hall": &RoomData{
			Description: "You are in a hallway that goes left and right near an airlock. To the left is a red light. There is a green button on the airlock.",
			Action: func(sd *StateData) {
				fmt.Printf("HP: %d\n", sd.Hero.HP)
			},
			ChoiceMap: map[string]*Choice{
				"A": &Choice{
					ChoiceDesc: "Push the button",
					ResultDesc: "",
					ResultRoom: "airlock"},
				"B": &Choice{
					ChoiceDesc: "Left down the hallway",
					ResultDesc: "You realize that you've just been dosed with radiation",
					ResultAction: func(sd *StateData) {
						sd.Hero.HP--
						fmt.Printf("HP: %d\n", sd.Hero.HP)
					},
					ResultRoom: "success"},
				"C": &Choice{
					ChoiceDesc: "Right down the hallway",
					ResultDesc: "",
					ResultRoom: "success"}}}}}
